# README #

This is an application for managing information for the sign in and sign out for Manzano Mesa Multigenerational Center


[Link to site](https://safe-dusk-7836.herokuapp.com/)

### Summary ###

* Create Sign In and Sign Out sheets
* Manage emergency contact information
* Store important information for children such as
    * Allergies
    * DOB
    * Emergency Contacts
    * Gender
    * Group Number
    * Count of Contacts
* Group information
    * Child count
    * Count of males and females for the specified group

#### To-do

* Add Mobile Sign-Out
    * Potentially Signature-pad-rails gem
    * Store Parent.id, Staff.id, [Children IDs]
    * Needs to be able to generate SignOut Forms at the end of each day
* Auto-Update Age field