class CreateChildrenAndContacts < ActiveRecord::Migration
  def change
    create_table :children_contacts, id: false do |t|
      t.belongs_to :child, index: true
      t.belongs_to :contact, index: true
    end
  end
end
