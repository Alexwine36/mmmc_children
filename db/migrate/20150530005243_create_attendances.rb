class CreateAttendances < ActiveRecord::Migration
  def change
    create_table :attendances do |t|
      t.integer :contact_id
      t.integer :child_id
      t.boolean :out
      t.integer :user_id
      t.text :signature

      t.timestamps null: false
    end
  end
end
