class AddEmergencyToContacts < ActiveRecord::Migration
  def change
    add_column :contacts, :emergency_contact, :boolean
  end
end
