class CreateChildren < ActiveRecord::Migration
  def change
    create_table :children do |t|
      t.string :last_name
      t.string :first_name
      t.integer :age
      t.integer :grade
      t.string :gender

      t.timestamps null: false
    end
  end
end
