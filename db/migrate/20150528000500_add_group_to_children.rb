class AddGroupToChildren < ActiveRecord::Migration
  def change
    add_column :children, :group_id, :integer
    add_column :children, :allergies, :text
  end
end
