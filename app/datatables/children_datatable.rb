class ChildrenDatatable
  delegate :params, :h, :link_to, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        sEcho: params[:sEcho].to_i,
        iTotalRecords: Child.count,
        iTotalDisplayRecords: children.total_entries,
        aaData: data
    }
  end

  private

  def data
    children.map do |child|
      [
          link_to(child.last_name, child),
          h(child.first_name),
          h(child.updated_at.strftime("%B %e, %Y"))
      ]
    end
  end

  def children
    @children ||= fetch_children
  end

  def fetch_children
    children = Child.order("#{sort_column} #{sort_direction}")
    children = children.page(page).per_page(per_page)
    if params[:sSearch].present?
      children = children.where("last_name like :search or first_name like :search", search: "%#{params[:sSearch]}%")
    end
    children
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[last_name first_name age gender]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end