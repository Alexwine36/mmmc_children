class ChildrenController < ApplicationController
   include ApplicationHelper
  before_action :set_child, only: [:show, :edit, :update, :destroy]

  # GET /children
  # GET /children.json
  def index
    # if params[:q].present?
      @q = Child.ransack(params[:q])
      @children = @q.result
    # else
      # @children = Child.all.order(:last_name)
      respond_to do |format|
        format.html
        format.csv do
          headers['Content-Disposition'] = "attachment; filename=\"user-list.csv\""
          headers['Content-Type'] ||= 'text/csv'
        end
        format.pdf do
          pdf = ChildrenPdf.new(@children, view_context)
          send_data pdf.render, filename: "children_#{Time.now}.pdf", type: "application/pdf", disposition: "inline"
        end
      # end
    end


  end

  # GET /children/1
  # GET /children/1.json
  def show
  end

  # GET /children/new
  def new
    @child = Child.new

  end

  # GET /children/1/edit
  def edit
  end

  # POST /children
  # POST /children.json
  def create
    
    @child = Child.new(child_params)
    @child.age = get_age(@child.dob)
    respond_to do |format|
      if @child.save
        format.html { redirect_to @child, notice: 'Child was successfully created.' }
        format.json { render :show, status: :created, location: @child }
      else
        format.html { render :new }
        format.json { render json: @child.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /children/1
  # PATCH/PUT /children/1.json
  def update

    respond_to do |format|
      if @child.update(child_params)
        @child.age = get_age(@child.dob)
        format.html { redirect_to @child, notice: 'Child was successfully updated.' }
        format.json { render :show, status: :ok, location: @child }
      else
        format.html { render :edit }
        format.json { render json: @child.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /children/1
  # DELETE /children/1.json
  def destroy
    @child.destroy
    respond_to do |format|
      format.html { redirect_to children_url, notice: 'Child was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def import
    Child.import(params[:file])
    redirect_to children_path, notice: "Children added successfully"
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_child
      @child = Child.find(params[:id])
      
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def child_params
      params.require(:child).permit(:last_name, :first_name, :age, :grade, :gender, :group_id, :allergies, :dob, :contact_ids => [])
    end
end
