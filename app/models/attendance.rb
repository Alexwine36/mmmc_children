class Attendance < ActiveRecord::Base
  belongs_to :contact
  belongs_to :child
  belongs_to :user
end
