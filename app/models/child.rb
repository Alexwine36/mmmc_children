class Child < ActiveRecord::Base
  # attr_accessible :last_name, :first_name

  belongs_to :group
  has_many :attendances
  has_and_belongs_to_many :contacts

  require 'roo'

  def self.import(file)
    xlsx = Roo::Spreadsheet.open(file)
    puts xlsx.info
    xlsx.each_with_index(:last_name => 'last_name', :first_name => 'first_name', :age => 'age', :grade => 'grade', :gender => 'gender', :group_id => 'group') do |row, i|
      child_hash = row.to_hash
      child = Child.where(last_name: child_hash[:last_name], first_name: child_hash[:first_name])
      child_find = Child.find_by(last_name: child_hash[:last_name], first_name: child_hash[:first_name])
      puts i
      if i == 0
        puts "Header"
      elsif child_find
        puts row.inspect + " already present"
        puts child
      else
        child.create(child_hash)
      end
    end
  end


end
