json.array!(@children) do |child|
  json.extract! child, :id, :last_name, :first_name, :age, :grade, :gender
  json.url child_url(child, format: :json)
end
