json.array!(@events) do |event|
  json.extract! event, :id, :name, :start, :end, :price, :groups, :notes
  json.url event_url(event, format: :json)
end
