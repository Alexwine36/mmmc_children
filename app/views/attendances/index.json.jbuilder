json.array!(@attendances) do |attendance|
  json.extract! attendance, :id, :contact_id, :child_id, :out, :user_id, :signature
  json.url attendance_url(attendance, format: :json)
end
