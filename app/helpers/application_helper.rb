module ApplicationHelper
  def copyright_notice_year_range(start_year)
    start_year = start_year.to_i

    current_year = Time.new.year

    if current_year > start_year && start_year > 2000
      "#{start_year} - #{current_year}"
    elsif start_year > 2000
      "#{start_year}"
    else
      "#{current_year}"
    end
  end

  def web_title(page_title = '')
    base_title = "MMMC"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

  def administrator
    if user_signed_in?
      :true if AdminUser.find_by_email(current_user.email)
    end
  end

  def duplicate_check(model)
    @model = model.select(:first_name,:last_name).group(:first_name, :last_name).having("count(*) > 1")
    # @model.each do |model|
    #   puts model.last_name
    #   puts model.first_name
    # end
  end

  def warning_facet(model, call = "errors")

    @model = model.all.order(:last_name, :first_name)
    model_error = []
    model_warn = []
    model_result = []


    if model == Child
      @model.each do |model|
        if model.contacts.count < 1
          model_error << {:last_name => model.last_name, :first_name => model.first_name, :link => model}
        end
        if model.contacts.count == 1
          model_warn << {:last_name => model.last_name, :first_name => model.first_name, :link => model}
        end
      end
    end


    if model == Contact
      @model.each do |model|
      if model.children.count < 1
        model_error << {:last_name => model.last_name, :first_name => model.first_name, :link => model}
      end
      if model.children.count == 1
        model_warn << {:last_name => model.last_name, :first_name => model.first_name, :link => model}
      end
      end

    end

    if call == "errors"
      model_result = model_error
    end

    if call == "warning"
      model_result = model_warn
    end
    model_result
  end
  
  def get_age(bd)
    # Get Age
    d = DateTime.now()
    # Difference in years, less one if you have not had a birthday this year.
    a = d.year - bd.year
    a = a - 1 if (
         bd.month >  d.month or 
        (bd.month >= d.month and bd.day > d.day)
    )
    a
  end

end
