module ContactsHelper
  def child_check(contact, tag, class_options = '')
    size = contact.children.size
    response = "<#{tag} "
    if size == 0
      response += "class='alert label round #{class_options}'>#{size}</#{tag}>"

    elsif size == 1
      response += "class='warning label round #{class_options}'>#{size}</#{tag}>"

    else
      response += "class='label round #{class_options}'>#{size}</#{tag}>"
    end
    response
  end
end
