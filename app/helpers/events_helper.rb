module EventsHelper
  def countdown(t)
    time = distance_of_time_in_words_to_now(t)
    time
  end
  def duration(event)
    time = distance_of_time_in_words(event.start, event.end)
    time
  end
  def next_event
    ticker = ""
    @events = Event.order(:start)

    i = -1;
    @events.each_with_index { |event, index|
    if event.start > Time.now && i == -1
      puts i
      i = index
      puts i
      ticker += "Next Event: #{event.name} | #{event.groups} -- #{countdown(event.start)}"
    end

      }
    # end
    ticker
  end
end
