module ChildrenHelper

  def contact_check(child, tag, class_options = '')
    size = child.contacts.size
    response = "<#{tag} "
    if size == 0
      response += "class='alert label round #{class_options}'>#{size}</#{tag}>"

    elsif size == 1
      response += "class='warning label round #{class_options}'>#{size}</#{tag}>"

    else
      response += "class='label round #{class_options}'>#{size}</#{tag}>"
    end

    response
  end



end
