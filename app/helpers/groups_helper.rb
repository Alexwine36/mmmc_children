module GroupsHelper
  def group_links
    @groups = Group.all.order(:id)
    @menu = []
    @groups.each do |group|
      @menu << group
    end
    @menu
  end
end
