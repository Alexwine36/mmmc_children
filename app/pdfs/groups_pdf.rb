class GroupsPdf<Prawn::Document
  class HighlightCallback
    def initialize(options)
      @color = options[:color]
      @document = options[:document]
    end

    def render_behind(fragment)
      original_color = @document.fill_color
      @document.fill_color = @color
      @document.fill_rectangle(fragment.top_left, fragment.width, fragment.height)
      @document.fill_color = original_color
    end
  end

  def initialize(groups, view)
    super(:page_layout => :landscape)

    @groups = groups
    @view = view

    @group_count = @groups.count

    puts @group_count

    @groups.each do |group|

      highlight = HighlightCallback.new(:color => group.color, :document => self)

      pad(0) { text "Date: ___________________________", :size => 15, :align => :right }
      formatted_text [{:text => group.name, :callback => highlight}], :size => 25, :align => :center
      pad(0) { text "Total: ________________", :size => 15, :align => :left }
      header = [["Child's Name", "Time In", "Parent Signature", "Time Out", "Parent Signature", "Staff", "Authorized Persons"]]
      data = header


      @children = group.children.order(:last_name, :first_name)

      @children.each do |f|
        authorized = ""
        f.contacts.each do |auth|
          authorized += "#{auth.first_name.titleize} #{auth.last_name.titleize}, "
        end
        cell_authorized = make_cell(:content => "#{authorized[0...-2]}", :size => 10)
        data += [["#{f.last_name}, #{f.first_name}", " ", "", "", "", "", cell_authorized]]
      end


      table(data, :header => true, :column_widths => [125, 59, 125, 59, 125, 56, 170], :row_colors => ["ffffff", "f0f0f0"]) do
        row(0).background_color = group.color

      end

      if @group_count > group.id
        start_new_page
      end
    end
  end
end