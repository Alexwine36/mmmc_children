class GroupPdf< Prawn::Document


  class HighlightCallback
    def initialize(options)
      @color = options[:color]
      @document = options[:document]
    end

    def render_behind(fragment)
      original_color = @document.fill_color
      @document.fill_color = @color
      @document.fill_rectangle(fragment.top_left, fragment.width, fragment.height)
      @document.fill_color = original_color
    end
  end

  def initialize(children, group, view)

    super(:page_layout => :landscape)

    highlight = HighlightCallback.new(:color => group.color, :document => self)

    # pad_top(20)
    pad(0) {text "Date: ___________________________", :size => 15, :align => :right }

    formatted_text [{:text => group.name, :callback => highlight}], :size => 25, :align => :center
    pad(0) { text "Total: ________________", :size => 15, :align => :left }

    @children = children
    @view = view

    header = [["Child's Name", "Time In", "Parent Signature", "Time Out", "Parent Signature", "Staff", "Authorized Persons"]]
    # header.fill("ff0000")
    # data = []
    # formatted_text [{:text => header, :callback => highlight}]
    data = header
    @children.each do |f|
      authorized = ""
      f.contacts.each do |auth|
        authorized += "#{auth.first_name} #{auth.last_name}, "
      end
      cell_authorized = make_cell(:content => "#{authorized[0...-2]}", :size => 10)
      data += [["#{f.last_name}, #{f.first_name}", " ", "", "", "", "", cell_authorized]]
      # stroke_horizontal_rule

    end
    # stroke_axis
    table(data, :header => true, :column_widths => [125, 59, 125, 59, 125, 56, 170], :row_colors => ["ffffff", "f0f0f0"]) do
      row(0).background_color = group.color
    end

   end
 end
