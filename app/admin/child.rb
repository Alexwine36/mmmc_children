ActiveAdmin.register Child do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
# permit(:last_name, :first_name, :age, :grade, :gender, :group_id, :allergies, :dob, :contact_ids => [])
permit_params :last_name, :first_name, :age, :grade, :gender, :group_id, :allergies, :dob

  # index download_links: [:csv, :xml, :json, :pdf]
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

 # scope("Issues") { |scope| scope.where(Child)}

  show :title => proc{ |child| "#{child.last_name}, #{child.first_name}" }


end
