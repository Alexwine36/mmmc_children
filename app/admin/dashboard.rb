ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    # div class: "blank_slate_container", id: "dashboard_default_message" do
    #   span class: "blank_slate" do
    #     span I18n.t("active_admin.dashboard_welcome.welcome")
    #     small I18n.t("active_admin.dashboard_welcome.call_to_action")
    #   end

    section do
      columns do
        column do
          panel "Recent Child Updates" do
            table_for Child.order("updated_at desc").limit(5) do
              column :last_name do |last|
                link_to last.last_name, admin_child_path(last)
              end
              column :first_name
              column :updated_at
              column :dob
            end
          end
        end
        column do
          panel "Recent Contact Updates" do
            table_for Contact.order("updated_at desc").limit(5) do
              column :last_name do |last|
                link_to last.last_name.titleize, admin_contact_path(last)
              end
              column :first_name do |first|
                first.first_name.titleize
              end
              column :updated_at
              column :phone
            end
          end
        end


      end
      columns do
        column do
          panel "Recent Event Updates" do
            table_for Event.order("updated_at desc").limit(5) do
              column :name do |name|
                link_to name.name, admin_event_path(name)
              end
              column :start
              column :end
              column :price
              column :groups
            end
          end
        end
      end


      # columns do
      #   column do
      #     panel "Potentail Issues" do
      #
      #     end
      #   end
      # end


      # columns do
      #   column do
      #     panel "Potential Issues" do
      #       @child = Child.all
      #
      #       table_for @child do
      #         @child.each do |child|
      #
      #           kid = child.contacts.count
      #           if kid > 0
      #             column :last_name
      #             column :first_name
      #           end
      #
      #         end
      #
      #
      #       end
      #     end
      #    end
      # end
  end

    # Here is an example of a simple dashboard with columns and panels.
    #
    # columns do
    #   column do
    #     panel "Recent Posts" do
    #       ul do
    #         Post.recent(5).map do |post|
    #           li link_to(post.title, admin_post_path(post))
    #         end
    #       end
    #     end
    #   end

    #   column do
    #     panel "Info" do
    #       para "Welcome to ActiveAdmin."
    #     end
    #   end
    # end
  end # content
  end
