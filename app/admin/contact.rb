ActiveAdmin.register Contact do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
 # permit_params contacts_ids: [:id], children_ids: [:id]
permit_params :first_name, :last_name, :phone, :email, :emergency_contact
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
  show :title => proc{ |contact| "#{contact.last_name}, #{contact.first_name}" }


end
